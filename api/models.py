from django.db import models

# Create your models here.
class Note(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
def __str__(self):
        return '%s %s' % (self.title, self.body)

class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age=models.CharField(max_length=2)
    username=models.CharField(max_length=10)
    ci=models.TextField()

class Pet(models.Model):
	name=models.CharField(max_length=30)
	owner_name=models.CharField(max_length=30)
	race=models.CharField(max_length=10)
	gender=models.CharField(max_length=10)
	age=models.CharField(max_length=10)
	"""
	def __str__(self):
    	return self.name
    	"""