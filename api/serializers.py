from rest_framework import serializers

from .models import Pet


class PetSerializer(serializers.Serializer):
	name=serializers.CharField(max_length=30)
	owner_name=serializers.CharField(max_length=30)
	race=serializers.CharField(max_length=10)
	gender=serializers.CharField(max_length=10)
	age=serializers.CharField(max_length=10)

	def create(self, validated_data):
		return Pet.objects.create(**validated_data)
	def update(self, instance, validated_data):
		instance.age = validated_data.get('age', instance.age)
		instance.gender = validated_data.get('gender', instance.gender)
		instance.race = validated_data.get('race', instance.race)
		instance.owner_name = validated_data.get('owner_name', instance.owner_name)
		instance.name = validated_data.get('name', instance.name)
		instance.save()
		return instance