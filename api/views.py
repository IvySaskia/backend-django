from django.shortcuts import render

# Create your views here.

#ver porque no da


from django.http import HttpResponse
from django.template import Context, loader

from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Pet
from .serializers import PetSerializer

def sign_up(request):
	return render(request,'sign_up.html')
	#otra forma (ver porque no da)
	"""
    template = loader.get_template("template\index.html")
    return HttpResponse(template.render())
    """
class PetView(APIView):
    def get(self, request):
        pets = Pet.objects.all()
        serializer = PetSerializer(pets, many=True)
        return Response({"pets": serializer.data})
    def post(self, request):
        pet = request.data.get('pet')
        serializer = PetSerializer(data=pet)
        if serializer.is_valid(raise_exception=True):
            pet_saved = serializer.save()
        return Response({"success": "Pet '{}' created successfully".format(pet_saved.name)})
    
    def put(self, request, pk):
        saved_pet = get_object_or_404(Pet.objects.all(), pk=pk)
        data = request.data.get('pet')
        serializer = PetSerializer(instance=saved_pet, data=data, partial=True)

        if serializer.is_valid(raise_exception=True):
            pet_saved = serializer.save()
        return Response({"success": "Pet '{}' updated successfully".format(pet_saved.name)})


    def delete(self, request, pk):
        pet = get_object_or_404(Pet.objects.all(), pk=pk)
        pet.delete()
        return Response({"message": "Pet with id `{}` has been deleted.".format(pk)},status=204)
