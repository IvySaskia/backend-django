# api/resources.py
from tastypie.resources import ModelResource
from api.models import Note
from api.models import Person
from tastypie.authorization import Authorization
class NoteResource(ModelResource):
    class Meta:
        queryset = Note.objects.all()#get
        resource_name = 'note'
        authorization = Authorization()#post,get,delete
        fields = ['title', 'body']

class PersonResource(ModelResource):
    class Meta:
        queryset = Person.objects.all()
        resource_name = 'person'
        authorization = Authorization()
